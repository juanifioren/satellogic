from django.conf import settings
from django.shortcuts import render

from .models import Task


def home(request, **kwargs):
    kwargs['pending'] = Task.objects.filter(state='PENDING', satelite_number__isnull=True)[:10]
    kwargs['last'] = Task.objects.exclude(state='PENDING', satelite_number__isnull=True)[:10]

    kwargs['satellites_stats'] = Task.get_satellites_stats()

    return render(request, 'home.html', kwargs)
