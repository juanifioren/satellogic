from django.db import models
from django.utils.translation import ugettext_lazy as _


class Task(models.Model):

    STATES = (
        ('DONE', 'Done'),
        ('FAILED', 'Failed'),
        ('PENDING', 'Pending'),
    )

    name = models.CharField(max_length=50, verbose_name=_('Name'))
    payoff = models.FloatField(verbose_name=_('Payoff'))
    satelite_number = models.PositiveSmallIntegerField(null=True, blank=True,
        verbose_name=_('Satelite Number'))
    resources = models.TextField(blank=True, default='', verbose_name=_('Resources'),
        help_text=_('Enter as comma-separated values.'))
    state = models.CharField(max_length=20, choices=STATES, default=STATES[2][0])
    date_created = models.DateTimeField(auto_now_add=True, verbose_name=_('Date Created'))
    last_update = models.DateTimeField(auto_now=True, verbose_name=_(u'Last Update'))

    def __str__(self):
        return self.name

    @property
    def resources_list(self):
        return self.resources.split(',')

    @classmethod
    def get_satellites_stats(cls):
        # Create counters with custom filters.
        total = models.Count('state', filter=models.Q(state__in=['DONE', 'FAILED']))
        done = models.Count('state', filter=models.Q(state='DONE'))
        failed = models.Count('state', filter=models.Q(state='FAILED'))

        # Query done and failed tasks per satellite.
        result = cls.objects.filter(satelite_number__isnull=False).values('satelite_number').order_by('satelite_number')\
            .annotate(total=total).annotate(done=done).annotate(failed=failed)

        return result
