import time

from django.test import TestCase

from .lib import Satellite
from .models import Task


class SatelliteTestCase(TestCase):

    def setUp(self):
        self.task_1 = Task.objects.create(name='fotos', payoff=10, resources='1,5')
        self.task_2 = Task.objects.create(name='mantenimiento', payoff=1, resources='1,2')
        self.task_3 = Task.objects.create(name='pruebas', payoff=1, resources='5,6')
        self.task_4 = Task.objects.create(name='fsck', payoff=0.1, resources='1,6')

    def test_tasks_distribution(self):
        tasks = Task.objects.all()
        satellites_tasks = Satellite._distribute_tasks(tasks)
        self.assertTrue(self.task_1 in satellites_tasks[1])
        self.assertTrue(self.task_2 in satellites_tasks[2])
        self.assertTrue(self.task_3 in satellites_tasks[2])
        self.assertTrue(not self.task_4 in satellites_tasks[1])
        self.assertTrue(not self.task_4 in satellites_tasks[2])
