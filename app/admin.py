from django.contrib import admin

from .lib import Satellite
from .models import *


def send_tasks_to_station(modeladmin, request, queryset):
    # Send tasks by excluding the ones that are already completed.
    Satellite.send_tasks_to_station(queryset.exclude(state__in=['DONE', 'FAILED']))

send_tasks_to_station.short_description = 'Send selected tasks to station'

def reset_tasks(modeladmin, request, queryset):
    for task in queryset:
        task.state = 'PENDING'
        task.satelite_number = None
        task.save()

reset_tasks.short_description = 'Reset selected tasks (pending again)'


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):

    actions = [send_tasks_to_station, reset_tasks]
    list_display = ['name', 'state', 'satelite_number', 'resources', 'date_created']
    search_fields = ['name']
    readonly_fields = ['state', 'date_created', 'last_update']
