from functools import reduce
from multiprocessing import (
    Pool,
    Process,
)
from random import random

from django.conf import settings


class Satellite:

    quantity = settings.SATELLITES_QUANTITY

    @classmethod
    def send_tasks_to_station(cls, tasks):
        """
        Create and start the main terrain station process.

        Return: None
        """
        station_process = Process(name='station', target=cls.process_station_tasks, args=(tasks,))
        station_process.start()

    @classmethod
    def process_station_tasks(cls, tasks):
        """
        Create a `Pool` and call each process in parallel.

        Return: None
        """
        satellites_tasks = cls._distribute_tasks(tasks)

        with Pool(processes=cls.quantity) as pool:
            # Use apply_async and wait later for the results.
            results = [ pool.apply_async(cls.process_satellite_tasks, (nm, ts)) \
                for nm, ts in satellites_tasks.items() ]
            # Now the calls are done in parallel.
            [r.get() for r in results]

    @classmethod
    def process_satellite_tasks(cls, number, tasks):
        """
        Iterate all the tasks and decide which one were completed or failed.

        Return: None
        """
        for task in tasks:
            task.satelite_number = number
            task.state = 'DONE' if random() > 0.1 else 'FAILED'
            task.save()

    @classmethod
    def _distribute_tasks(cls, tasks):
        """
        Decide which task will be assigned to available processes.

        Return: None
        """
        satellites_tasks = { k:[] for k in range(1, cls.quantity + 1) }

        # Distribute tasks between satellites.
        for task in tasks:
            for satellite_number in satellites_tasks.keys():
                # Current resources asigned to this satellite.
                res_of_sate = reduce(lambda x,y: x+y,
                    [l.resources_list for l in satellites_tasks[satellite_number]], [])
                # Check if a satellite is already using a resource.
                if not set(task.resources_list).intersection(set(res_of_sate)):
                    satellites_tasks[satellite_number].append(task)
                    break

        return satellites_tasks
