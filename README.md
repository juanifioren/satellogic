# Satellite

## About

Stack used:

* Python 3.6.3
* Django 2.0.1

**Why django?**
Well, after reading the bonus points I thought it could be good using django features like admin, actions, ORM and more to rapidly tackle some common functionalities of the app.

**Why `multiprocessing`?**
To be honest, I always use Celery when I have to manage asynchronous tasks. So, even though I've played before with the `multiprocessing` module, is not common for me using it.
I use `multiprocessing.Process` to handle the station job and `multiprocessing.Pool` to have parallel execution of satellites jobs.

## Installation

Setup project environment with [virtualenv](https://virtualenv.pypa.io) and [pip](https://pip.pypa.io).

```bash
$ virtualenv -p python3 project_env

$ source project_env/bin/activate

$ git clone https://bitbucket.org/juanifioren/satellogic.git
$ cd satellogic
$ pip install -r requirements.txt
```

Run your the app.

```bash
$ python manage.py migrate
$ python manage.py createsuperuser
$ python manage.py runserver
```

Open your browser and go to `http://localhost:8000`.

Run unit test.

```bash
$ python manage.py test
```

## Functionalities

* **View pendings** in the home page.
* **View recent operations** in the home page.
* **Create tasks** by clicking the "Create Task" button.
* **Send tasks to station** by clicking the "Manage Tasks" button, select the tasks you wanna send and then choose the action called "Send selected tasks to station".
* **Reset tasks** by clicking the "Manage Tasks" button, select the tasks you wanna send and then choose the action called "Reset selected tasks".
* **Basic satellites statistics** counting tasks executed, completed and failed per satellite.
* **Custom satellites numbers** edit the setting `SATELLITES_QUANTITY` inside `satellogic/settings.py` to change the numbers of satellites that will handle tasks execution.

## Things to improve

* **Create custom logger for processes** have logger for station and satellites processes.
* **Improve exception handling** giving more failure tolerance.

Created by Juan Ignacio Fiorentino. 02 Feb 2018.
